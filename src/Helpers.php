<?php
/**
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 * 2006
 *
 */

namespace angelrove\utils;

class Helpers
{
    //------------------------------------------------------------------
    public static function exportTocsv(
        array $listFields,
        array $listRows,
        $SEP,
        $isDebug = false,
        $fileName = 'export.csv'
    )
    {
        $LINE_RET = ($isDebug) ? '<br>' : "\n";

        //--------------
        $header = '';
        foreach ($listFields as $f_name) {
            $header .= $SEP . '"' . $f_name . '"';
        }
        $header = ltrim($header, $SEP);
        $header .= $LINE_RET;

        //--------------
        $str_csv = '';
        foreach ($listRows as $row) {
            $line = '';
            foreach ($listFields as $f_name) {
                $value = trim($row[$f_name]);

                if (!$isDebug) {
                    $value = addslashes($value);
                }
                $value = str_replace(array("\n", "\r"), ' ', $value);

                $line .= $SEP . '"' . $value . '"';
            }
            $line = ltrim($line, $SEP);
            $str_csv .= $line . $LINE_RET;
        }

        // OUT ---------
        if ($isDebug) {
            return $header . $str_csv;
        } else {
            // header('Content-type: application/zip');
            header('Content-Disposition: attachment; filename="' . $fileName . '"');
            // header("Pragma: no-cache");
            // header("Expires: 0");
            echo $header . $str_csv;
            exit();
        }
    }
    //------------------------------------------------------------------
    public static function get_imageFromUrl(
        $url_image,
        $path_uploads,
        $dir_upload,
        $name_file,
        $max_with = false,
        $def_ext = ''
    ): array
    {
        // Image name ---
        $ext = pathinfo($url_image, PATHINFO_EXTENSION);
        list($ext) = explode('?', $ext);

        if ($def_ext && (strlen($ext) > 4 || !$ext)) {
            $ext = $def_ext;
        }

        $img_name = "$name_file.$ext";

        // Image path ---
        $path_upload = $path_uploads . '/' . $dir_upload . '/';
        $full_path = $path_upload . $img_name;

        // Save image file ---
        if (($ret = @file_get_contents($url_image)) === false) {
            // throw new \Exception("Failed to open url image [$url_image]");
            return false;
        }
        file_put_contents($full_path, $ret);

        // Resize ------------
        if ($max_with) {
            list($width, $height, $tipo, $atributos) = getimagesize($full_path);
            if ($width > $max_with) {
                \angelrove\utils\Images\ImageTransform::resize($path_upload, $img_name, $max_with);
            }
        }

        // str membrillo -----
        // $image_mime = image_type_to_mime_type(exif_imagetype($full_path));
        $ret = [
            'img_name' => $img_name,
            'dir_upload' => $dir_upload
        ];

        return $ret;
    }
    //------------------------------------------------------------------
    /**
     * Llamar antes de cargar cualquier vista para así minimizar el riesgo de
     * cachear errores(ya que al volcarse un warning no se enviarían los headers)
     */
    public static function headersCacheTime($min, $hours = 0, $days = 0)
    {
        if ($days)
            $days = 60 * 60 * 24 * $days;
        if ($hours)
            $hours = 60 * 60 * $hours;
        if ($min)
            $min = 60 * $min;

        // fecha
        $expires = $min + $hours + $days;

        // headers
        header("Cache-Control: public");
        header("Pragma: public");
        header("Cache-Control: maxage=" . $expires);
        header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT');

        return '<pre style="font-size:20px">
    DEBUG - cache_headers_enable()<hr>
    fecha   = [' . gmdate('d/m/Y H:i:s', time()) . '] GMT
    expires = [' . gmdate('d/m/Y H:i:s', time() + $expires) . '] GMT >> ' . $expires . 'sec.
    ' . gmdate('D, d M Y H:i:s', time() + $expires) . '
    </pre>';
    }
//------------------------------------------------------------------
}